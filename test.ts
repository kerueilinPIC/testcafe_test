import { Selector } from "testcafe"; // 引入 testcafe 的 html element 選擇器

fixture`Getting Started`.page`http://devexpress.github.io/testcafe/example`; // 1. 進入 TestCafe Example Page

const name_input = Selector("#developer-name");

test("Your name: init Test", async t => {
  await t
    // 2. 直接使用 html element id，識別 input 在 Your name 輸入 ice
    .expect(name_input.value).eql("");
})

test("Your name: Test", async t => {
  const name_input = Selector("#developer-name");
  await t
    // 2. 直接使用 html element id，識別 input 在 Your name 輸入 ice
    .typeText(name_input, "ice")
    .expect(name_input.value).eql("ice");
})

test("Which features are important to you: init Test", async t => {
  const supportRemoteBox = Selector("#remote-testing");
  const reusing = Selector("#reusing-js-code");
  const background = Selector("#background-parallel-testing");
  const continuous = Selector("#continuous-integration-embedding");
  const traffic = Selector("#traffic-markup-analysis");
  await t
    .expect(supportRemoteBox.checked).notOk()
    .expect(reusing.checked).notOk()
    .expect(background.checked).notOk()
    .expect(continuous.checked).notOk()
    .expect(traffic.checked).notOk();
  })

test("Which features are important to you: Test", async t => {
  const supportRemoteBox = Selector("#remote-testing");
  const reusing = Selector("#reusing-js-code");
  const background = Selector("#background-parallel-testing");
  const continuous = Selector("#continuous-integration-embedding");
  const traffic = Selector("#traffic-markup-analysis");
  await t
    .click(supportRemoteBox)
    .expect(supportRemoteBox.checked).ok()
    .expect(reusing.checked).notOk()
    .expect(background.checked).notOk()
    .expect(continuous.checked).notOk()
    .expect(traffic.checked).notOk();
  })

test("What is your primary Operating System: init Test", async t => {
  // 使用 TestCafe 鏈式 Selector API
  const macOSRadio = Selector(".column")
  .find("label")
  .withText("MacOS")
  .child("input");
  const windowRadio = Selector(".column")
  .find("label")
  .withText("MacOS")
  .child("input");
  const linuxRadio = Selector(".column")
  .find("label")
  .withText("MacOS")
  .child("input");
  await t
    .expect(macOSRadio.checked).notOk()
    .expect(windowRadio.checked).notOk()
    .expect(linuxRadio.checked).notOk();
})

test("What is your primary Operating System: Test", async t => {
  // 使用 TestCafe 鏈式 Selector API
  const macOSRadio = Selector(".column")
  .find("label")
  .withText("MacOS")
  .child("input");
  await t
    .click(macOSRadio)
    .expect(macOSRadio.checked).ok();
})

test("Which TestCafe interface do you use:init Test", async t => {
  const interfaceSelect = Selector("#preferred-interface");
  await t
    // 5.1 透過預定義的 selector interfaceSelect，按下選單彈出選項
    .expect(interfaceSelect.value).eql("Command Line")
})

test("Which TestCafe interface do you use: Test", async t => {
  const interfaceSelect = Selector("#preferred-interface");
  await t
    // 5.1 透過預定義的 selector interfaceSelect，按下選單彈出選項
    .expect(interfaceSelect.value).eql("Command Line")
    .click(interfaceSelect)
    // 5.2 使用 ＴestCafe 鏈式 Selector API，找到 interfaceSelect 下的 Both Option
    .click(interfaceSelect.find("option").withText("Both"))
    .expect(interfaceSelect.value).eql("Both");
})

test("Which TestCafe interface do you use: Test", async t => {
  const interfaceSelect = Selector("#preferred-interface");
  await t
    // 5.1 透過預定義的 selector interfaceSelect，按下選單彈出選項
    .expect(interfaceSelect.value).eql("Command Line")
    .click(interfaceSelect)
    // 5.2 使用 ＴestCafe 鏈式 Selector API，找到 interfaceSelect 下的 Both Option
    .click(interfaceSelect.find("option").withText("Both"))
    .expect(interfaceSelect.value).eql("Both");
})

const slider = Selector("#slider");
const comments = Selector("#comments");
const slider_handle = Selector(".ui-slider-handle");
test("I have tried TestCafe unChecked: Test", async t => {
  await t
    .expect(slider.hasClass("ui-state-disabled")).ok()
    .expect(comments.hasAttribute("disabled")).ok()
    .expect(comments.value).eql("")
    .expect(slider_handle.getStyleProperty("left")).eql("0px");
})

test("I have tried TestCafe Checked: Test", async t => {
  const TestCafe_button = Selector("#tried-test-cafe");
  await t
    .click(TestCafe_button)
    .typeText(comments, "123")
    .expect(slider.hasClass("ui-state-disabled")).notOk()
    .drag(slider_handle, 270, 0, { offsetX: 10, offsetY: 10 })
    .expect(comments.hasAttribute("disabled")).notOk()
    .expect(comments.value).eql("123")
    .expect(slider_handle.getStyleProperty("left")).eql("270.484px");
})