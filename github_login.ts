import { Selector } from "testcafe";

try{
    require('dotenv').config();
} catch (err) {
    console.log("docker image no dotenv");
}
   
const test_URL :string = String(process.env.URL);
fixture`Getting Started`.page(test_URL);

console.log('USER_NAME', process.env.USER_NAME);
console.log('USER_PASSWORD', process.env.USER_PASSWORD);

test('log-in and do stuff', async t => {
    const user_login = Selector("#login_field");
    const user_password = Selector("#password");
    const sign_in = Selector(".js-sign-in-button");
    const login_verify = Selector(".auth-form-header").child("h1");
    const login_success = Selector(".css-truncate-target")
    const USER_NAME : string = String(process.env.USER_NAME);
    const USER_PASSWORD : string = String(process.env.USER_PASSWORD);
    await t
        .click(user_login)
        .typeText(user_password, USER_PASSWORD)
        .typeText(user_login, USER_NAME)
        .click(sign_in);
        // the test continues
        try{
            await t.expect(login_success.innerText).eql(USER_NAME);
        } catch {
            await t.expect(login_verify.innerText).eql("Device verification");
            console.log("需要驗證");
        }
        console.log('USER_NAME', process.env.USER_NAME);
        console.log('Development mode: ', process.env.YOUR_VARIABLE);
});

//sudo docker run -e USER_PASSWORD='Kk963741' -e USER_NAME='kerueilin999' -it -v $PWD:/tests testcafe/testcafe chromium /tests/github_login.ts /bin/bash
//sudo docker run --rm -e USER_PASSWORD='9574kkk9574' -e USER_NAME='kerueilin9' -it -v $PWD:/tests testcafe/testcafe chromium /tests/github_login.ts

//rm:
//sudo docker run --rm -e USER_PASSWORD='Kk963741' -e USER_NAME='kerueilin999' -it -v $PWD:/tests testcafe/testcafe chromium /tests/github_login.ts