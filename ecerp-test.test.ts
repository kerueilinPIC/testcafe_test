import { ClientFunction, Selector } from "testcafe";

try{
    require('dotenv').config();
} catch (err) {
    console.log("docker image no dotenv");
}
   
const test_URL :string = String(process.env.URL);
fixture`Getting Started`.page(test_URL);

console.log('USER_NAME', process.env.USER_NAME);

const user_login = Selector(".n-form-item-blank")
.nth(0)
.find('input');
const user_password = Selector(".n-form-item-blank")
.nth(1)
.find('input');
const sign_in = Selector("button");
const login_success = Selector("h2")
const USER_NAME : string = String(process.env.USER_NAME);
const USER_PASSWORD : string = String(process.env.USER_PASSWORD);

test('log-in succeeded', async t => {
    await t
        .typeText(user_password, USER_PASSWORD)
        .typeText(user_login, USER_NAME)
        .click(sign_in)

        // test Assertions

        .expect(login_success.innerText).eql('多通路中台');
});

test('log-in succeeded: use access_token', async t => {
    const getLocalStorageItem = ClientFunction((key: string) : string|null => {return window.localStorage.getItem(key)});
    const ACCESS_TOKEN = getLocalStorageItem("ACCESS-TOKEN");
    await t
        .typeText(user_password, USER_PASSWORD)
        .typeText(user_login, USER_NAME)
        .click(sign_in)

        // test Assertions
        .expect(ACCESS_TOKEN).ok();
        // console.log(ACCESS_TOKEN);
});

test('log-in failed', async t => {
    const errorMsg = Selector('.n-message__content').innerText;

    await t
        .typeText(user_password, 'errorPassword')
        .typeText(user_login, USER_NAME)
        .click(sign_in)

        // test Assertions
        .expect(errorMsg).eql('帳號密碼錯誤');
});

//n-form-item-feedback__line

test('log-in failed: wrong format for account', async t => {
    const errorMsg = Selector('.n-form-item-feedback__line').innerText;

    await t
        .typeText(user_password, USER_PASSWORD)
        .typeText(user_login, "USER_NAME")

        // test Assertions
        .expect(errorMsg).eql('請填入正確格式的 Email');
});

test('log-in failed: wrong format for password', async t => {
    const errorMsg = Selector('.n-form-item-feedback__line').innerText;

    await t
        .typeText(user_password, "d")
        .typeText(user_login, USER_NAME)

        // test Assertions
        .expect(errorMsg).eql('請輸入大於六位數的密碼');
});

test('log-in failed: wrong format for all', async t => {
    const errorMsg_login = Selector('.n-form-item-feedback__line').innerText;
    const errorMsg_password = Selector('.n-form-item-feedback__line').nth(1).innerText;
    const errorMsg_alert = Selector('.n-message__content').innerText;

    await t
        .typeText(user_password, "d")
        .typeText(user_login, "USER_NAME")
        .click(sign_in)

        // test Assertions
        .expect(errorMsg_alert).eql("請填完整登入訊息")
        .expect(errorMsg_login).eql('請填入正確格式的 Email')
        .expect(errorMsg_password).eql('請輸入大於六位數的密碼');
}).skipJsErrors();
