import { Selector } from "testcafe"; // 引入 testcafe 的 html element 選擇器

fixture('Getting Started').page('http://devexpress.github.io/testcafe/example'); // 1. 進入 TestCafe Example Page

const name_input = Selector("label");

test("test", async t => {
  await t
    .expect(name_input.innerText).eql("Support for testing on remote devices");
    console.log('Development mode: ', process.env.DEV_MODE);
})