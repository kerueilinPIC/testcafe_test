# testcafe

## test.ts

## 測試流程
1. 先在 local 端測試成功
2. 到 docker 上進行測試

### 測試內容：

對testcafe的範例網頁做以下測試：

1. 測試 input 輸入框
1. 測試多選勾選
1. 測試單選勾選
1. 測試下拉選單
1. 測試enable or disable
1. 利用t.drag測試slider

## code

### init
引入 testcafe 的 html element 選擇器：  
`import { Selector } from "testcafe";`

進入範例網頁：
```
fixture`Getting Started`.page`http://devexpress.github.io/testcafe/example`; // 1. 進入 TestCafe Example Page
```


### selector：
`const selector = Selector("#id")`  
`const selector = Selector(".class")`
```
.find("label")
.withText("context")
.child("input")
```

### Test Actions：
點擊：  
`t.click(selector);`

### Assertions：
表單勾選：
```
t.expect(selector.checked).ok();
t.expect(selector.checked).notOk();
```

### 較難部分：
t.drag：    
`t.drag(selector(元件), x(x軸移動x px), y(y軸移動y px), {offsetX, offsetY}(點擊元件內的位置))`，    
例如：`t.drag('#button', 100, 50, {offsetX：5, offsetY：5})`， 點擊 button 往右下移動。

---

## github_login.ts

### 測試內容：

成功登入 github 

### command：
用於設定環境變數：  
`npm install dotenv --save`

### code：

引入 dotenv 套件：  
`require('dotenv').config();`

讀取環境變數(xxx是你在 .env 裡面設定的變數)：   
`process.env.xxx`

---

## ecerp-test.test.ts

### 測試內容：

1. 成功登入統一多通路中台
1. 帳號密碼錯誤測試
1. 帳號格式錯誤測試
1. 密碼格式錯誤測試
1. 帳號密碼格式同時錯誤測試

### code：

引入 testcafe 套件(ClientFunction 為取得 local_storage data)：  
`import { ClientFunction, Selector } from "testcafe";`

忽略所有 JavaScript 錯誤：  
`.skipJsErrors();`

取得 local_storage data：   
```
const getLocalStorageItem = ClientFunction((key: string) : string|null => {return window.localStorage.getItem(key)});
const ACCESS_TOKEN = getLocalStorageItem("ACCESS-TOKEN");
```


